<?php
[insert_php]
$mypod = pods( 'vivencias' );
$mypod = pods( 'vivencias', $id_or_slug );
$params = array('limit' =>15);
$mypod = pods( 'vivencias', $params );
$mypod = pods( 'vivencias' )->find( $params );
$mypod = pods( 'vivencias' );
$mypod->find( $params );
echo '<div class="row">';
echo '<div class="small-11 small-centered medium-11 large-10 columns">';
echo '<div class="row">';
$cont = 0;
while ( $mypod->fetch() ) {

	echo '<div class="medium-6 large-4 columns text-center pods-vivencia">';
	echo '<a href="'.$mypod->field('title').'" data-reveal-id="myModal'.$cont .'"><h4>'.$mypod->field('title').'</h4></a>';
	echo '<img src="'.$mypod->display('imagen').'" alt="" />';
	echo '<h5>'.truncate($mypod->display('descripcion'),120).'</h5>';
	echo '<h5><span>'.$mypod->display('autor').'</span></h5>';

	echo '<div id="myModal'.$cont .'" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">';
  	echo '<h2 id="modalTitle">'.$mypod->field('title').'</h2>';
  	echo '<p class="lead">'.$mypod->field('content').'</p>';
  	echo '<a class="close-reveal-modal" aria-label="Close">&#215;</a>';
	echo '</div>';

	echo '</div>';
	$cont +=1;



}
echo '</div>';
echo '</div>';
echo '</div>';
[/insert_php]


[insert_php]

$mypod = pods( 'imagenes' );
$mypod = pods( 'imagenes', $id_or_slug );
$params = array('limit' =>15);
$mypod = pods( 'imagenes', $params );
$mypod = pods( 'imagenes' )->find( $params );
$mypod = pods( 'imagenes' );
$mypod->find( $params );
echo '<div class="row">';
echo '<div class="small-11 small-centered medium-11 large-10 columns">';
echo '<div class="row">';
while ( $mypod->fetch() ) {
	if ($mypod->display('mobile')!="Yes"){
		echo '<ul class="small-block-grid-3 ">';
		echo '<li>';

		echo '<div class="cont">';
		echo '<div class="img-cont">';
		echo '<img src="'.$mypod->display('imagen').'" alt="'.$mypod->field('title').'" />';
		echo '</div>';
		echo '<div class="row">';
		echo '<div class="small-12 medium-6 text-left columns">';
		echo '<h5><span>Tamaño: '.$mypod->display('size').'</span></h5>';
		echo '</div>';
		echo '<div class="small-12 medium-6 text-right columns">';
		echo '<a href="'.$mypod->display('imagen').'" download="'.$mypod->field('title').'">Descarga</a>';
		echo '</div>';
		echo '</div>';
		echo '</div>';

		echo '</li>';
		echo '</ul>';

	}

}
echo '</div>';
echo '</div>';
echo '</div>';

[/insert_php]

[insert_php]

$mypod = pods( 'imagenes' );
$mypod = pods( 'imagenes', $id_or_slug );
$params = array('limit' =>15);
$mypod = pods( 'imagenes', $params );
$mypod = pods( 'imagenes' )->find( $params );
$mypod = pods( 'imagenes' );
$mypod->find( $params );
echo '<div class="row">';
echo '<div class="small-11 small-centered medium-11 large-10 columns">';
echo '<div class="row">';
while ( $mypod->fetch() ) {
	if ($mypod->display('mobile')!="Yes"){
	echo '<div class="medium-6 large-4 columns pods-imagen">';
	echo '<div class="cont">';
	echo '<div class="img-cont">';
	echo '<img src="'.$mypod->display('imagen').'" alt="'.$mypod->field('title').'" />';
	echo '</div>';
	echo '<div class="row">';
	echo '<div class="small-12 medium-6 text-left columns">';
	echo '<h5><span>Tamaño: '.$mypod->display('size').'</span></h5>';
	echo '</div>';
	echo '<div class="small-12 medium-6 text-right columns">';
	echo '<a href="'.$mypod->display('imagen').'" download="'.$mypod->field('title').'">Descarga</a>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	}

}
echo '</div>';
echo '</div>';
echo '</div>';

[/insert_php]

[insert_php]

$mypod = pods( 'imagenes' );
$mypod = pods( 'imagenes', $id_or_slug );
$params = array('limit' =>15);
$mypod = pods( 'imagenes', $params );
$mypod = pods( 'imagenes' )->find( $params );
$mypod = pods( 'imagenes' );
$mypod->find( $params );
echo '<div class="row">';
echo '<div class="small-11 small-centered medium-11 large-10 columns">';
echo '<div class="row">';
while ( $mypod->fetch() ) {
	if ($mypod->display('mobile')=="Yes"){
	echo '<div class="medium-4 large-3 columns pods-imagen">';
	echo '<div class="cont mb">';
	echo '<div class="img-cont-mb">';
	echo '<img src="'.$mypod->display('imagen').'" alt="'.$mypod->field('title').'" />';
	echo '</div>';
	echo '<div class="row">';
	echo '<div class="small-12 medium-6 text-left columns">';
	echo '<h5><span>Tamaño: '.$mypod->display('size').'</span></h5>';
	echo '</div>';
	echo '<div class="small-12 medium-6 text-right columns">';
	echo '<a href="'.$mypod->display('imagen').'" download="'.$mypod->field('title').'">Descarga</a>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	}

}
echo '</div>';
echo '</div>';
echo '</div>';

[/insert_php]

?>






	echo '<div class="medium-6 large-4 columns pods-imagen">';
	echo '<div class="cont">';
	echo '<div class="img-cont">';
	echo '<img src="'.$mypod->display('imagen').'" alt="'.$mypod->field('title').'" />';
	echo '</div>';
	echo '<div class="row">';
	echo '<div class="small-12 medium-6 text-left columns">';
	echo '<h5><span>Tamaño: '.$mypod->display('size').'</span></h5>';
	echo '</div>';
	echo '<div class="small-12 medium-6 text-right columns">';
	echo '<a href="'.$mypod->display('imagen').'" download="'.$mypod->field('title').'">Descarga</a>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';




[insert_php]
	$mypod = pods( 'links' );
	$mypod = pods( 'links', $id_or_slug );
	$params = array('limit' =>15);
	$mypod = pods( 'links', $params );
	$mypod = pods( 'links' )->find( $params );
	$mypod = pods( 'links' );
	$mypod->find( $params );


	<div class="row cbackground" id="id8a">
		<div class="vertical-align-container vac-id8a vac">
			<div  class="vertical-align-content text-center vac-id8a">
				<div class="small-12  small-centered  columns" >
					<div class="row">
						while ( $mypod->fetch() ) {
						<div class="small-11 small-centered  medium-6 medium-uncentered columns">
							<div class="row">
								<a href="" >
								<div class="small-8 columns">

									<p class="titulo">Calendario</p>
									<p>Encuentra la programación de actividades que se realizaran en la Sala IEEE</p>

								</div>
								<div  class="small-4 columns icom">
									<div class="small-6 small-left columns cont_icom">
									<i class="fa fa-calendar"></i>
									</div>
								</div>
								</a>
							</div>
						</div>
						}
					</div>
				</div>
			</div>
		</div>
	</div>


	echo '<div class="row">';
	echo '<div class="small-11 small-centered medium-11 large-10 columns">';
	echo '<div class="row">';
	while ( $mypod->fetch() ) {
		echo '<div class="medium-6 large-4 columns text-center pods-vivencia">';
		echo '<a href="#" data-reveal-id="myModal"><h4>'.$mypod->field('title').'</h4></a>';
		echo '<img src="'.$mypod->display('imagen').'" alt="" />';
		echo '<h5>'.truncate($mypod->display('descripcion'),120).'</h5>';
		echo '<h5><span>'.$mypod->display('autor').'</span></h5>';

		echo '<div id="myModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">';
	  	echo '<h2 id="modalTitle">'.$mypod->field('title').'</h2>';
	  	echo '<p class="lead">'.$mypod->field('content').'</p>';
	  	echo '<a class="close-reveal-modal" aria-label="Close">&#215;</a>';
		echo '</div>';

		echo '</div>';


	}
	echo '</div>';
	echo '</div>';
	echo '</div>';




[insert_php]
$mypod = pods( 'premios' );
$mypod = pods( 'premios', $id_or_slug );
$params = array('limit' =>15);
$mypod = pods( 'premios', $params );
$mypod = pods( 'premios' )->find( $params );
$mypod = pods( 'premios' );
$mypod->find( $params );
<div class="row premios">
<div class="small-12  small-centered medium-11 columns" >
<div class="row">
while ( $mypod->fetch() ) {
<div class="small-10 small-centered  medium-4 medium-uncentered columns pods-imagen">
<div class="small-12 columns img-cont-mb">
<img src="[insert_php] echo $mypod->field('imagen'); [/insert_php]">
</div >
<div class="small-12 medium-6 text-left columns">
<p>$mypod->field('titulo')</p>
</div>
</div>
}
</div>
</div>
</div>
[/insert_php]