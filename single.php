<?php get_header(); ?>

<?php include ('wp-content/themes/IEEE/' . '/breadcrumbs.php'); ?>


<div class="row">
    <!-- loop de Noticias -->
    <div class="">
        <article>

            <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
                <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Enlace permanente a <?php the_title_attribute(); ?>"><?php the_title(); ?>. </a></h2>

                <small class="postdate" style="padding-left:50px;"><img src="<?php bloginfo('template_url'); ?>/images/date.png" /> <?php the_time('F jS, Y') ?>. Categoría: <?php the_category(', '); ?> </small>

                <div>
                   <?php the_content(); ?>
               </div>

           <?php endwhile; ?>
       <?php endif; ?>
   </article> <!-- Fin de single -->
</div>
</div>
<?php get_footer(); ?>
