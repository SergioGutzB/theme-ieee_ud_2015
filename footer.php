 <div class="row" id="id10"  >
		<div class="vertical-align-container vac-id10">
			<div  class="vertical-align-content text-center vac-id10">
				<div class="small-12 small-centered columns">
					<div class="row">
						<div class="small-12 small-centered medium-uncentered medium-6 large-7 columns">
							<h5>Diseñamos esta página inspirados en las experiencias que hemos vivido en IEEE, buscando dejar una herencia a nuestra querida rama y de paso animarte por medio de testimonios y hechos reales a que te unas y nos acompañes en esta gran aventura. Hemos impreso toda nuestra energía y creatividad en este trabajo, esperamos que lo disfrutes y que sea agradable para ti.</h5>
							<h5 class="text-right"><span>Sergio Gutiérrez, Cristian Quintero - Departamento de Diseño IEEE-UD</span></h5>
						</div>
						<div class="small-12 small-centered medium-uncentered medium-6 large-5 columns">
							<div class="row row-logos">
								<div class="small-4 medium-4 columns">
									<a href="" title=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/100_ESPA.png" alt=""></a>
								</div>
								<div class="small-4 medium-4 columns">
									<a href="" title=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/LogoRama.png" alt=""></a>
								</div>
								<div class="small-4 medium-4 columns">
									<a href="" title=""><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/IEEE.png" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php wp_footer(); ?>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/foundation/js/foundation.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/foundation/js/foundation/foundation.magellan.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slider-host/slick.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/slider.general.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/caman.full.min.js">
</body>
</html>
