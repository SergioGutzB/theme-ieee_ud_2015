<?php get_header(); ?>

<section class="main">

	<?php
	$img = get_post_custom_values("banner");
	if ($img[0] != "")	{ ?>
	<div class="row cbackground header-page clearfix" style="background-image: url(<?= $img[0]?>); min-height: 640px; background-attachment: scroll; height:640px;">
		<div class="vertical-align-container vac-id1">
			<div  class="vertical-align-content vac-id1"  data-stellar-ratio="1.5">
				<div class="row">
					<div class="small-5 small-centered  medium-3 large-2 columns text-center">
						<img class="img-rama" src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/ChamizoCircuilar.png" alt="">
					</div>
					<div  class="samll-12 small-centered text-center columns page-title">
						<div class="medium-8 medium-centered columns">
							<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
								<h1><?php the_title(); ?></h1>
							<?php endwhile; else: ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row row-social">
		<div class="small-12 small-centered columns buttons">
			<div class="small-12 small-centered columns buttons">
				<a href="https://www.facebook.com/RamaIEEEud" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Facebook.png">
				</a>
				<a href="https://twitter.com/IEEEUD" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Twitter.png">
				</a>
				<a href="https://instagram.com/ieee.ud/" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Instagram.png">
				</a>
				<a href="https://www.youtube.com/user/RamaIEEEud" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/YouTube.png">
				</a>
			</div>
		</div>
	</div>


</div>
<?php } ?>


<!-- <?php include ('wp-content/themes/IEEE_UD/' . '/breadcrumbs.php'); ?> -->

<div class="row">
	<!-- loop de Noticias -->
	<div class="samll-12 small-centered medium-12 columns vac">
		<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>

			<?php the_content(); ?>
			<!-- 	<?php the_time('F jS, Y') ?> -->
		</br>
	<?php endwhile; else: ?>
	<h2>No encontrado</h2>
	<p>Lo sentimos, intente utilizar nuestro formulario de b&uacute;squedas.</p>
	<br>
	<br>
<?php endif; ?>
</div><!-- end of wrapper-->
</div>


<div class="slider-general">
	 		<!-- <div class="bg-slider" style="background: transparent url('<?php echo get_stylesheet_directory_uri(); ?>/img/Fotos_Antiguas/Comite_Ejecutivo_2001.jpg') repeat scroll center center / cover  !important;">
	 		</div>
			<div class="bg-slider" style="background: transparent url('<?php echo get_stylesheet_directory_uri(); ?>/img/Fotos_Antiguas/Comite_Ejecutivo_001_b.jpg') repeat scroll center center / cover  !important;">
			</div> -->

			<?php
			$slider = get_post_custom_values("slider");
			if ($slider!=NULL)
			foreach ($slider as $slide) {
				?>

				<div class="bg-slider" style="background: transparent url('<?php echo $slide ?>') repeat scroll center top / cover  !important;">
				</div>

				<?php
			} ?>

		</div>



	</section>
	<?php get_footer(); ?>
