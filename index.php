<?php get_header(); ?>

<section id="main">
	<div id="id1" class="row cbackground clearfix" >
		<div class="insignia">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/InsigniaPuestoES.png" alt="">
		</div>

		<div class="vertical-align-container vac-id1">
			<div  class="vertical-align-content vac-id1"  data-stellar-ratio="1.5">
				<div class="row">
					<div class="small-3 small-centered columns text-center">
						<img class="img-rama" src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/ChamizoCircuilar.png" alt="">
					</div>
				</div>
				<div class="row">
					<div class="small-6 small-centered columns text-center img-logo">
						<img class="img-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/ViveLaExperienciaConSombra.png" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="row row-social">
			<div class="small-12 small-centered columns buttons">
				<a href="https://www.facebook.com/RamaIEEEud" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Facebook.png">
				</a>
				<a href="https://twitter.com/IEEEUD" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Twitter.png">
				</a>
				<a href="https://instagram.com/ieee.ud/" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Instagram.png">
				</a>
				<a href="https://www.youtube.com/user/RamaIEEEud" target="_blank" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/YouTube.png">
				</a>
			</div>
		</div>

		<!-- <div class="row row-lenguaje">
			<div class="small-12 small-centered columns">
				<a href="http://ieee.ud" >
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/BotonES.png">
				</a>
				<a href="http://ieee.ud.en">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/BotonEN.png">
				</a>
			</div>
		</div> -->
	</div>

	<div class="row" id="id2" >
		<div class="vertical-align-container vac-id2 vac">
			<div  class="vertical-align-content text-center vac-id2">
				<div class="small-12  medium-8 small-centered  columns " >
					<h3 class="titles">Somos un Equipo</h3>
					<h4>Estableciendo un camino para contribuir con el desarrollo de nuestra sociedad.</h4>
					<br>
					<p >Pertenecientes a la Universidad Distrital “Francisco José de Caldas”, como equipo compartimos un mismo objetivo: Contribuir al desarrollo profesional de estudiantes y de la comunidad educativa, brindando espacios para el desarrollo de liderazgo y trabajo interdisciplinar, lo cual da como resultado voluntarios comprometidos con sus labores y nos permite ser reconocidos como una de las ramas más sobresalientes a nivel nacional, regional y mundial.</p>
					<br>
					<a href="/equipo/" class="button gris" >DESCUBRE MÁS</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row cbackground" id="id3" >
		<div class="vertical-align-container vac-id3 vac">
			<div  class="vertical-align-content text-center vac-id3">
				<div class="small-11 medium-8 small-centered columns txt-blanco" >
					<h3 class="titles">Más de 50 años</h3>
					<h4 >Aportando líderes a la sociedad</h4>
					<br>
					<p >La llegada del hombre a la luna, la invención del Microprocesador Intel 4004, el lanzamiento del Telstar I y la primera transmisión de televisión vía satélite fueron algunos de los sucesos que marcaron una década muy importante para nuestra sociedad y fue esta misma en la cual fue fundada nuestra rama.</p>
					<br>
					<a href="/historia/" class="button blanco">NUESTRA HISTORIA</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="id4">
		<div class="vertical-align-container vac-id4  vac">
			<div  class="vertical-align-content text-center vac-id4">
				<div class="small-12 medium-11 small-centered columns">
					<h3 class="titles">Conferencias y Eventos.</h3>
					<h4>Promoviendo el desarrollo de la investigación e innovación</h4>
					<br>
					<div class="row">
						<div class="small-11 small-centered medium-4 medium-uncentered columns">
							<a href="http://ieee.udistrital.edu.co/rap/" target="_blank"><img src="http://ieee.udistrital.edu.co/wp-content/uploads/2015/07/robot_al_parque.jpg" alt="Robot Al Parque 2015" class="img-event"></a>
							<h3 class="stitle-event">Robot al Parque® 2015</h3><p>
								 Sé parte de uno de los mejores concursos de Robótica en Bogotá, no te pierdas este gran evento que llega en su versión número diez
							</p>

						</div>
						<div class="small-11 small-centered medium-4 medium-uncentered columns">
							<img  src="http://ieee.udistrital.edu.co/wp-content/uploads/2015/06/OpenCongress.jpg" alt="Open Congress® 2015" class="img-event">
							<h3 class="stitle-event">Open Congress® 2015</h3>
							<p >Un evento que promueve la interdisciplinariedad en conocimientos de ingeniería con el fin de ayudar a mejorar la vida de los colombianos</p>
						</div>
						<div class="small-11 small-centered medium-4 medium-uncentered columns">
							<a href="http://ieee.udistrital.edu.co/wea/" target="_blank"><img  src="http://ieee.udistrital.edu.co/wp-content/uploads/2015/06/WEA.jpg" alt="Workshop on Engineering." class="img-event"></a>
							<h3 class="stitle-event">Workshop on Engineering</h3>
							<p>
							El WEA promueve la divulgación científica. Nos permite conocer la calidad de nuestras investigaciones exponiéndolas junto con los mejores trabajos de la región.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row cbackground" id="id5" >
		<div class="vertical-align-container vac-id5 vac">
			<div  class="vertical-align-content text-center vac-id5">
				<div class="small-12  small-centered medium-6 columns txt-blanco" >
					<h3 class="titles">Vivencias™</h3>
					<h4 >Nuestro Blog</h4>
					<br>
					<p >Asombrosos Viajes, conferencias inigualables, anécdotas únicas y todos los acontecimientos en un solo lugar. Haz clic y lee  lo que vivimos día a día.</p>
					<br>
					<a href="/vivencias/" class="button blanco">EMPIEZA A LEER</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="id6">
		<div class="vertical-align-container vac-id6 vac">
			<div  class="vertical-align-content text-center vac-id6">
				<div class="small-12 small-centered columns">
					<h3 class="titles">Rama Tv™</h3>
					<h4>La creatividad como parte esencial</h4>
					<br>

					<iframe width="1024" height="580" src="https://www.youtube-nocookie.com/embed/EWHegJjpKHA?hd=1&amp;rel=0&amp;autohide=1&amp;showinfo=0" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" yplugin="true" style="display: block;"></iframe>
					<br>
					<br>
					<a href="https://www.youtube.com/user/RamaIEEEud" target="_blank" class="button gris">MIRA MÁS VIDEOS</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row cbackground" id="id7">
		<div class="vertical-align-container vac-id7 vac">
			<div  class="vertical-align-content text-center vac-id7">
				<div class="small-12 small-centered medium-7 columns txt-blanco" >
					<h3 class="titles">IEEE</h3>
					<h4 >La asociación profesional más grande del mundo</h4>
					<br>
					<p >
						Toma el siguiente paso y vive una experiencia más allá de la académica, en la cual podrás aplicar las habilidades que aprendas dentro y fuera del aula para que puedas liderar e impactar el mundo que te rodea. Aprovecha de todos los beneficios que IEEE tiene para ti y haz nuevos amigos que aportarán a tu futuro profesional
					</p>
					<br>
					<a href="http://ieee.org" target="_blank" class="button blanco">CONOCE MÁS</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="id8"  name="capitulos">
		<div class="vertical-align-container vac-id8 vac" id="capitulos">
			<div  class="vertical-align-content text-center vac-id8">
				<div class="small-12 small-centered medium-8 columns">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/LogoUneteAzul.png" alt="">
					<br>
					<br>
					<p>
						Cuando ingresas a nuestra rama, además de un grupo de trabajo encuentras una familia que siempre estará dispuesta a apoyarte para alcanzar tus metas; por esta razón nos encantaría contar contigo para que seas parte de asombrosos proyectos
					</p>
				</div>
				<div class="small-12 small-centered medium-11 columns ">
					<br>
					<div class="row">
						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="/aess-2" title="Capítulo AESS IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/AESS.png" alt="Capitulo - AESS - IEEEUD">
							</a>
							<p>AESS</p>
						</div>
<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="Capítulo CAS IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/CAS.png" alt="Capítulo - CSS - IEEEUD">
							</a>
							<p>CAS</p>
						</div>

<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="Capítulo Computer IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/Computer.png" alt="Capítulo - CSS - IEEEUD">
							</a>
							<p>COMPUTER</p>
						</div>
					</div>
					<div class="row">

<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="Capítulo ComSoc IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/ComSoc.png" alt="Capítulo - CSS - IEEEUD">
							</a>
							<p>COMSOC</p>
						</div>

						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="/css-2" title="Capítulo CSS IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/CSS.png" alt="Capítulo - CSS - IEEEUD">
							</a>
							<p>CSS</p>
						</div>

<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="#" title="Capítulo EDS IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/EDS.png" alt="Capítulo - CSS - IEEEUD">
							</a>
							<p>EDS</p>
						</div>

</div>
					<div class="row">

						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="/emb-2" title="Capítulo EMB IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/EMB.png" alt="Capitulo - EMB - IEEEUD">
							</a>
							<p>EMB</p>
						</div>
					
						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="/grss-2" title="Capitulo IEEE-UD">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/GRSS.png" alt="Capitulo - GRSS - IEEEUD">
							</a>
							<p>GRSS</p>
						</div>
						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="/ias-2" title="Capitulo IAS IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/IAS.png" alt="Capitulo - IAS - IEEEUD">
							</a>
							<p>IAS</p>
						</div>

</div>
					<div class="row">
						<div class="small-10  small-centered medium-4 medium-uncentered  columns image-circle">
							<a href="/pels-2" title="Capítulo PELS IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/PELS.png" alt="Capitulo - PELS - IEEEUD">
							</a>
							<p>PELS</p>
						</div>
					
						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="/pes-2" title="Capítulo PES IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/PES.png" alt="Capitulo - PES - IEEEUD">
							</a>
							<p>PES</p>
						</div>
						<div class="small-10  small-centered medium-4 medium-uncentered columns image-circle">
							<a href="/ras-2" title="Capítulo RAS - IEEE UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/RAS.png" alt="Capitulo - RAS - IEEEUD">
							</a>
							<p>RAS</p>
						</div>

</div>
					<div class="row">
						<div class="small-10  small-centered medium-4 medium-centered columns image-circle">
							<a href="/wie" title="Capítulo WIE IEEE-UD" target="_blank">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/Capitulos/WIE.png" alt="Capitulo - WIE - IEEEUD">
							</a>
							<p>WIE</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- <div class="row cbackground" id="id8a">
		<div class="vertical-align-container vac-id8a vac">
			<div  class="vertical-align-content text-center vac-id8a">
				<div class="small-12  small-centered  columns" >
					<div class="row">
						<div class="small-11 small-centered  medium-6 medium-uncentered columns">
							<div class="row">
								<a href="" >
								<div class="small-8 columns">

									<p class="titulo">Calendario</p>
									<p>Encuentra la programación de actividades que se realizaran en la Sala IEEE</p>

								</div>
								<div  class="small-4 columns icom">
									<div class="small-6 small-left columns cont_icom">
									<i class="fa fa-calendar"></i>
									</div>
								</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

<?php
	$mypod = pods( 'links' );
	$mypod = pods( 'links', $id_or_slug );
	$params = array('limit' =>15);
	$mypod = pods( 'links', $params );
	$mypod = pods( 'links' )->find( $params );
	$mypod = pods( 'links' );
	$mypod->find( $params );

?>
	<div class="row cbackground" id="id8a">
		<div class="vertical-align-container vac-id8a vac">
			<div  class="vertical-align-content text-center vac-id8a">
				<div class="small-12  small-centered  columns" >
					<div class="row">
						<?php while ( $mypod->fetch() ) { ?>
						<div class="small-11 small-centered  medium-6 medium-uncentered columns">
							<div class="row">
								<a href="<?= $mypod->display('link') ?>">
								<div class="small-8 columns">

									<p class="titulo"><?= $mypod->display('titulo') ?></p>
									<p><?= $mypod->display('descripcion') ?></p>
								</div>
								<div  class="small-4 columns icom">
									<div class="small-6 small-left columns cont_icom">
									<i class="fa fa-<?= $mypod->display('icono') ?>"></i>
									</div>
								</div>
								</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row cbackground" id="id9">
		<div class="vertical-align-container vac-id9">
			<div  class="vertical-align-content text-center vac-id9">
				<div class="small-12 small-centered medium-10 columns">
					<div class="row">
						<div class="small-12 medium-4 left columns recuadro" id="download-center">
							<h3>Centro de Descargas</h3>
							<p>Además de documentos, actas, trabajos, proyectos y demás archivos académicos que encontrarás, hemos diseñado una serie de Wallpapers para que los uses en tus dispositivos y lleves la #ExperienciaIEEE siempre contigo.</p>
							<a href="/centro-de-descargas/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Iconos/MultiDevice.png" alt="Descargas"></a>
						</div>

						<div class="small-12 medium-4 columns" id="timeline-tw">
							<div>
								<a class="twitter-timeline" href="https://twitter.com/ieeeud" data-widget-id="625719222971334656">Tweets por el @ieeeud.</a>
								<script>
							!function(d,s,id){
								var js,fjs=d.getElementsByTagName(s)[0],
								p=/^http:/.test(d.location)?'http':'https';
								if(!d.getElementById(id)){js=d.createElement(s);
									js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
									fjs.parentNode.insertBefore(js,fjs);
								}
							}(document,"script","twitter-wjs");
							</script>
							</div>
						</div>



						<div class="small-12 medium-4 right columns recuadro" id="questions">
							<h3>¿Aún con dudas?</h3>
							<p>Tenemos un equipo dispuesto a ayudarte con tus preguntas y capaz de asesorarte en la mayoría de los temas que necesites. Anímate y escríbenos  cómo podemos ayudarte.</p>
							<a href="/contactanos/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Iconos/contacto.png" alt="Descargas"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</section> <!-- Fin de main -->

<?php get_footer(); ?>
