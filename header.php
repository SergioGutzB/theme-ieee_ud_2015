<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<!-- <meta charset="	<?php bloginfo( 'charset' ); ?>"> -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title><?php wp_title(); ?></title>

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

	<link rel="stylesheet" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/app.css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/page.css" />
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick.css" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/contact.css" />


	<script src="<?php echo get_stylesheet_directory_uri(); ?>/bower_components/modernizr/modernizr.js" type="text/javascript"></script>

	<!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
    <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
    <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
<![endif]-->

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php wp_head(); ?>

</head>
<body>

<div class="panel-head">
<div class="fixed">
<div class="row row-top-bar">

    <div class="contain-to-grid sticky">
        <div class="row">
        <div class="small-12 medium-center columns" id="menu-ieee">
            <ul id="links-ieee">
                <li><a href="http://www.ieee.org/index.html" target="_blank">IEEE.org</a></li>
                <li><a href="http://ieeexplore.ieee.org/" target="_blank">IEEE Xplore Digital Library</a></li>
                <li><a href="http://standards.ieee.org/" target="_blank">IEEE Standards</a></li>
                <li><a href="http://ieeexplore.ieee.org/" target="_blank">IEEE Spectrum</a></li>
                <li><a href="http://www.ieee.org/sitemap.html" target="_blank">More Sites</a></li>
            </ul>
        </div>
    </div>
        <nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name logo-1">
                    <a href="http://ieee.org" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Logos/IEEE_azul.png" alt="logo"></a>
                </li>
                <li class="name logo-fixed">
                    <a href="/" ><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/chamizoc.png" alt="logo"></a>
                </li>
                <li class="toggle-topbar menu-icon"><a href="" target="_blank"><span>.</span></a></li>
            </ul>
            <section class="top-bar-section">

                <ul class="left" >

                    <li class="divider"></li>
                    <li class="<?= ($action == 'events') ? 'active' : ''; ?>"><a href="/Equipo/" >Equipo</a></li>
                    <li class="divider"></li>
                    <li class="<?= ($action == 'historia') ? 'active' : ''; ?>"><a href="/historia/" >Historia</a></li>
                    <li class="divider"></li>
                    <li class="<?= ($action == 'about') ? 'active' : ''; ?>"><a href="/vivencias/" >Vivencias</a></li>
                    <li class="divider"></li>
                    <li class="<?= ($action == 'news') ? 'active' : ''; ?>"><a href="https://www.youtube.com/user/RamaIEEEud" target="_blank" >Rama Tv</a></li>
                    <li class="divider"></li>
                    <li class="<?= ($action == 'reasons_to_ride') ? 'active' : ''; ?>" ><a href="http://ieee.udistrital.edu.co#capitulos"  class="smooth">Capítulos</a></li>
                    <li class="divider"></li>
                </ul>

                <ul class="right inline-list">

                    <li class="li-2">
                        <a href="/">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/BotonES.png" alt="" class="btn-idioma">
                        </a>
                    </li>
                    <li class="li-3">
                        <a href="/en/">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Botones/BotonEN.png" alt="" class="btn-idioma">
                        </a>
                    </li>
                </ul>
            </section>
        </nav>
    </div>
</div>
</div>
</div>