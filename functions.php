<?php
// Registro del menú de WordPress

add_theme_support( 'nav-menus' );

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'main' => 'Main'
			)
		);
}

function listar_vivencias(){
			// Get the Pods object
	$mypod = pods( 'vivencias' );

// Get the Pods object for a specific item
	$mypod = pods( 'vivencias', $id_or_slug );

// Get the Pods object and run find()
	$params = array(
		'limit' => 15
		);

	$mypod = pods( 'vivencias', $params );

// The above is the same as
	$mypod = pods( 'vivencias' )->find( $params );

// And the above is the same as
	$mypod = pods( 'vivencias' );
	$mypod->find( $params );

// Loop through the items returned
	while ( $mypod->fetch() ) {
		echo '<p>' . $mypod->display( 'autor' ) . '</p>';
	}

}
add_shortcode ('vivencias', 'listar_vivencias');

function shortcode_poema() {
$versos = "Que grande constipação física! / Preciso de verdade e de aspirina.";
$versos = htmlentities ($versos);
$versos = str_replace ("/","<br />",$versos);
$versos = "<blockquote>".$versos."</blockquote>";
return $versos;
}
add_shortcode('poemas', 'shortcode_poema');

function truncate ($str, $length=30, $trailing='...') {
        //    Cadena a truncar, Longitud a truncar (por defecto 30), Cadena a añadir (por defecto puntos suspensivos)

        if (strlen($str)>$length) { //Si el largo de la cadena supera lo que especificamos
            $pals = explode (' ',$str); //Crea un array donde cada elemento es una  palabra
            $str2='';
            foreach ($pals as $pal){  //Recorre el array y va agregando cada palabra en $str2 hasta que el largo de $str2 supera el largo maximo
              $ant  = $str2;
              $str2 = $str2.' '.$pal;
              if (strlen($str2)>$length){
                $str2=$ant . $trailing;
                break;
              }
            }
            return $str2;
        } else { //Si la cadena está dentro del tamaño especificado la devuelve sin hacer cambios
            return $str;
        }
    }

    if ( ! isset( $content_width ) )
	$content_width = 625;


?>